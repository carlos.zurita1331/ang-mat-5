import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form5',
  templateUrl: './form5.component.html',
  styleUrls: ['./form5.component.css']
})
export class Form5Component implements OnInit {

  array:string[]=[];
  formulario!:FormGroup;
  foco:boolean=false;
  mensaje!:string;
  constructor(private fb:FormBuilder) { 
    this.crearFormulario();
  }

  get ObtenerNombre(){
    return this.formulario.get('nombre');
  }

  get ObtenerCheckbox(){
    return this.formulario.get('CheckboxNombre') as FormArray;
  }


  ngOnInit(): void {
  }

  crearFormulario(){
    this.formulario = this.fb.group({
      nombre: ['',[Validators.required,Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      CheckboxNombre:this.fb.array([]), 
    })

   
  }

  Guardar():void{
    this.array=[];
    for (let i = 0; i < this.ObtenerCheckbox.length; i++) {
      if (this.ObtenerCheckbox.at(i).get('check')?.value ==true ) {
        console.log(this.ObtenerCheckbox.at(i).get('check'));
        
        this.foco=false;
        this.array.push( this.ObtenerCheckbox.at(i).get('nombre')?.value);
      }
    }
    
    
    if (this.array.length==0) {
      this.foco=true;
      setTimeout(() => {
        this.foco=false;
      }, 2000);
    } 
    console.log(this.array);
  }



  LimpiarCB(id:number):void{
    this.ObtenerCheckbox.removeAt(id);
  }

  clearBox():void{
    this.array=[];
  }

  Adicion():void{
    
    if (this.ObtenerNombre?.valid) {
      const nuevo = this.fb.group({
        check: [null],
        nombre : [this.formulario.get('nombre')?.value]
      });

      console.log(nuevo.get('name')?.value);
      
      this.ObtenerCheckbox.push(nuevo);
    } else {
      return;
    }
  }

  Mostrar():string{
    let resp='';
       this.array.forEach((e,i)=>{
          resp += `Id: ${i} - Nombre: ${e}\n`;
      });      
    return resp;
  }

}
